# "*syt5-sommer-semester-kurbaniec-tgm*"

### Embedded Devices

* [GEK1012 Embedded Devices "Time-critical data-transmission" (BORM)](https://bitbucket.org/kurbaniec/syt5-gek1012-time-critical-data-transmission/src/master/)

### Datamanagement

* [GEK103x Datamanagement "Distributed Data Structures" (MICT/BORM)](https://bitbucket.org/kurbaniec/syt5-gek103-distributed-data-structures/src/master/)

### Dezentrale Systeme

* [GEK1033 Dezentrale Systeme "Mobile Application" fplayer (BORM)](https://bitbucket.org/kurbaniec/syt5-gek1033-mobile-application-fplayer/src/master/)